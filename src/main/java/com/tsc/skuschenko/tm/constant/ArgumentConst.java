package com.tsc.skuschenko.tm.constant;

public interface ArgumentConst {

    String VERSION = "-v";

    String HELP = "-h";

    String ABOUT = "-a";

    String INFO = "-i";

}
