package com.tsc.skuschenko.tm.constant;

public interface InformationConst {

    String OPERATION_OK = "ok";

    String OPERATION_FAIL = "fail";

    String INDEX = "index";

    String ID = "id";

    String NAME = "name";

    String DESCRIPTION = "description";

    String SORT = "sort";

    String STATUS = "status";

    String PROJECT_ID = "project id";

    String TASK_ID = "task id";

}
