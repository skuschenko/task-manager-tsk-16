package com.tsc.skuschenko.tm.controller;

import com.tsc.skuschenko.tm.api.controller.ITaskController;
import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.constant.InformationConst;
import com.tsc.skuschenko.tm.constant.TerminalConst;
import com.tsc.skuschenko.tm.enumerated.Sort;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.entity.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    public TaskController(
            final ITaskService taskService,
            final IProjectTaskService projectTaskService
    ) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showTasks() {
        showOperationInfo(TerminalConst.TASK_LIST);
        showParameterInfo(InformationConst.SORT);
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks = new ArrayList<>();
        if (sort == null || sort.isEmpty()) tasks = taskService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void create() {
        showOperationInfo(TerminalConst.TASK_CREATE);
        showParameterInfo(InformationConst.NAME);
        final String name = TerminalUtil.nextLine();
        showParameterInfo(InformationConst.DESCRIPTION);
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void clear() {
        showOperationInfo(TerminalConst.TASK_CLEAR);
        taskService.clear();
    }

    @Override
    public void startTaskById() {
        showOperationInfo(TerminalConst.TASK_START_BY_ID);
        showParameterInfo(InformationConst.ID);
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(value);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startTaskByIndex() {
        showOperationInfo(TerminalConst.TASK_START_BY_INDEX);
        showParameterInfo(InformationConst.INDEX);
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startTaskByIndex(value);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void startTaskByName() {
        showOperationInfo(TerminalConst.TASK_START_BY_NAME);
        showParameterInfo(InformationConst.NAME);
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(value);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void completeTaskById() {
        showOperationInfo(TerminalConst.TASK_FINISH_BY_ID);
        showParameterInfo(InformationConst.ID);
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(value);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void completeTaskByIndex() {
        showOperationInfo(TerminalConst.TASK_FINISH_BY_INDEX);
        showParameterInfo(InformationConst.INDEX);
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(value);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void completeTaskByName() {
        showOperationInfo(TerminalConst.TASK_FINISH_BY_NAME);
        showParameterInfo(InformationConst.NAME);
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.completeTaskByName(value);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);

    }

    @Override
    public void changeTaskStatusById() {
        showOperationInfo(TerminalConst.TASK_CHANGE_STATUS_BY_ID);
        showParameterInfo(InformationConst.ID);
        final String valueId = TerminalUtil.nextLine();
        Task task = taskService.findOneById(valueId);
        if (task == null) throw new TaskNotFoundException();
        task = taskService.changeTaskStatusById(
                valueId, readTaskStatus()
        );
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void changeTaskStatusByIndex() {
        showOperationInfo(TerminalConst.TASK_CHANGE_STATUS_BY_INDEX);
        showParameterInfo(InformationConst.INDEX);
        final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        Task task = taskService.findOneByIndex(valueIndex);
        if (task == null) throw new TaskNotFoundException();
        task = taskService.changeTaskStatusByIndex(
                valueIndex, readTaskStatus()
        );
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void changeTaskStatusByName() {
        showOperationInfo(TerminalConst.TASK_CHANGE_STATUS_BY_NAME);
        showParameterInfo(InformationConst.NAME);
        final String value = TerminalUtil.nextLine();
        Task task = taskService.findOneByName(value);
        if (task == null) throw new TaskNotFoundException();
        task = taskService.changeTaskStatusByName(
                value, readTaskStatus()
        );
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showTaskById() {
        showOperationInfo(TerminalConst.TASK_VIEW_BY_ID);
        showParameterInfo(InformationConst.ID);
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(value);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void showTaskByIndex() {
        showOperationInfo(TerminalConst.TASK_VIEW_BY_INDEX);
        showParameterInfo(InformationConst.INDEX);
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(value);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void showTaskByName() {
        showOperationInfo(TerminalConst.TASK_VIEW_BY_NAME);
        showParameterInfo(InformationConst.NAME);
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(value);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void removeTaskById() {
        showOperationInfo(TerminalConst.TASK_REMOVE_BY_ID);
        showParameterInfo(InformationConst.ID);
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(value);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeTaskByIndex() {
        showOperationInfo(TerminalConst.TASK_REMOVE_BY_INDEX);
        showParameterInfo(InformationConst.INDEX);
        final Integer value = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeOneByIndex(value);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeTaskByName() {
        showOperationInfo(TerminalConst.TASK_REMOVE_BY_NAME);
        showParameterInfo(InformationConst.NAME);
        final String value = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(value);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void updateTaskById() {
        showOperationInfo(TerminalConst.TASK_UPDATE_BY_ID);
        showParameterInfo(InformationConst.ID);
        final String valueId = TerminalUtil.nextLine();
        Task task = taskService.findOneById(valueId);
        if (task == null) throw new TaskNotFoundException();
        showParameterInfo(InformationConst.NAME);
        final String valueName = TerminalUtil.nextLine();
        showParameterInfo(InformationConst.DESCRIPTION);
        final String valueDescription = TerminalUtil.nextLine();
        task = taskService.updateOneById(
                valueId, valueName, valueDescription
        );
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void updateTaskByIndex() {
        showOperationInfo(TerminalConst.TASK_UPDATE_BY_INDEX);
        showParameterInfo(InformationConst.INDEX);
        final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        Task task = taskService.findOneByIndex(valueIndex);
        if (task == null) throw new TaskNotFoundException();
        showParameterInfo(InformationConst.NAME);
        final String valueName = TerminalUtil.nextLine();
        showParameterInfo(InformationConst.DESCRIPTION);
        final String valueDescription = TerminalUtil.nextLine();
        task = taskService.updateOneByIndex(
                valueIndex, valueName, valueDescription
        );
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void findAllTaskByProjectId() {
        showOperationInfo(TerminalConst.FIND_ALL_TASK_BY_PROJECT_ID);
        showParameterInfo(InformationConst.PROJECT_ID);
        final String value = TerminalUtil.nextLine();
        final List<Task> tasks = projectTaskService.findAllTaskByProjectId(value);
        if (tasks == null || tasks.size() == 0) {
            throw new TaskNotFoundException();
        }
        tasks.forEach(item -> showTask(item));
    }

    @Override
    public void bindTaskByProject() {
        showOperationInfo(TerminalConst.BIND_TASK_BY_PROJECT);
        showParameterInfo(InformationConst.PROJECT_ID);
        final String projectId = TerminalUtil.nextLine();
        showParameterInfo(InformationConst.TASK_ID);
        final String taskId = TerminalUtil.nextLine();
        Task task = projectTaskService.bindTaskByProject(projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    @Override
    public void unbindTaskFromProject() {
        showOperationInfo(TerminalConst.BIND_TASK_BY_PROJECT);
        showParameterInfo(InformationConst.PROJECT_ID);
        final String projectId = TerminalUtil.nextLine();
        showParameterInfo(InformationConst.TASK_ID);
        final String taskId = TerminalUtil.nextLine();
        Task task = projectTaskService.unbindTaskFromProject(projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

    private void showTask(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
        System.out.println("START DATE: " + task.getDateStart());
        System.out.println("END DATE: " + task.getDateFinish());
    }

    private void showOperationInfo(final String info) {
        System.out.println("[" + info.toUpperCase() + "]");
    }

    private void showParameterInfo(final String info) {
        System.out.println("ENTER " + info.toUpperCase() + ":");
    }

    private Status readTaskStatus() {
        showParameterInfo(InformationConst.STATUS);
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        return status;
    }

}