package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.ILogService;
import com.tsc.skuschenko.tm.constant.LogConstant;

import java.io.IOException;
import java.util.logging.*;

public class LogService implements ILogService {

    private final Logger commands = Logger.getLogger(LogConstant.COMMANDS);

    private final Logger messages = Logger.getLogger(LogConstant.MESSAGES);

    private final Logger errors = Logger.getLogger(LogConstant.ERRORS);

    private final Logger root = Logger.getLogger("");

    private final LogManager manager = LogManager.getLogManager();

    {
        init();
        registry(commands, LogConstant.COMMANDS_FILE, false);
        registry(messages, LogConstant.MESSAGES_FILE, true);
        registry(errors, LogConstant.ERRORS_FILE, true);
    }

    public void registry(
            final Logger logger, final String filename, final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.info(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        messages.fine(message);
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            manager.readConfiguration(
                    LogService.class.getResourceAsStream(
                            LogConstant.LOG_PROPERTIES
                    )
            );
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

}
